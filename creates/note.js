const sample = require('../samples/sample_note');
const url = 'http://ec2-18-221-73-13.us-east-2.compute.amazonaws.com:8000';

const createIssue = (z, bundle) => {
    const responsePromise = z.request({
        method: 'POST',
        url: `${process.env.API_URL || url}/notes`,
        body: {
            title: bundle.inputData.title,
            text: bundle.inputData.text
        }
    });
    return responsePromise
        .then(response => JSON.parse(response.content));
};

module.exports = {
    key: 'note',
    noun: 'Note',

    display: {
        label: 'Create Note',
        description: 'Creates a note.'
    },

    operation: {
        inputFields: [
            {key: 'title', label: 'Title', required: true},
            {key: 'text', label: 'Text', required: false}
        ],
        perform: createIssue,
        sample: sample
    }
};
