const noteCreate = require('./creates/note');
const noteTrigger = require('./triggers/note');
const authentication = require('./authentication');

const handleHTTPError = (response, z) => {
  if (response.status >= 400) {
    throw new Error(`Unexpected status code ${response.status}`);
  }
  return response;
};


// To include the API key on all outbound requests, simply define a function here.
// It runs runs before each request is sent out, allowing you to make tweaks to the request in a centralized spot.
const includeApiKey = (request, z, bundle) => {
    if (bundle.authData.apiKey) {
        request.params = request.params || {};
        // request.params.api_key = bundle.authData.apiKey;
        //
        request.headers.Authorization = `Bearer ${bundle.authData.apiKey}`;
        // (If you want to include the key as a header instead)
        //
    }
    return request;
};

const App = {
  // This is just shorthand to reference the installed dependencies you have. Zapier will
  // need to know these before we can upload
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,
  authentication: authentication,

  // beforeRequest & afterResponse are optional hooks into the provided HTTP client
  beforeRequest: [
      includeApiKey
  ],

  afterResponse: [
    handleHTTPError
  ],

  // If you want to define optional resources to simplify creation of triggers, searches, creates - do that here!
  resources: {
  },

  // If you want your trigger to show up, you better include it here!
  triggers: {
    [noteTrigger.key]: noteTrigger,
  },

  // If you want your searches to show up, you better include it here!
  searches: {
  },

  // If you want your creates to show up, you better include it here!
  creates: {
    [noteCreate.key]: noteCreate,
  }
};

// Finally, export the app.
module.exports = App;



