'use strict';
const should = require('should');

const zapier = require('zapier-platform-core');

const App = require('../index');
const appTester = zapier.createAppTester(App);

describe('App.authentication.test', () => {

    it('passes authentication and returns json', (done) => {

        const bundle = {
            authData: {
                apiKey: process.env.TEST_API_KEY
            }
        };

        appTester(App.authentication.test, bundle)
            .then((json_response) => {
                // json_response.should.have.property('username');
                should.exist(json_response.label);
                done();
            })
            .catch(done);

    });

});


// describe('basic authentication', () => {
//     // Put your test TEST_USERNAME and TEST_PASSWORD in a .env file.
//     // The inject method will load them and make them available to use in your
//     // tests.
//     zapier.tools.env.inject();
//
//     it('should authenticate', (done) => {
//         const bundle = {
//             authData: {
//                 username: process.env.TEST_USERNAME,
//                 password: process.env.TEST_PASSWORD
//             }
//         };
//
//         appTester(App.authentication.test, bundle)
//             .then((response) => {
//                 should.exist(response.login);
//                 done();
//             })
//             .catch(done);
//     });
//
// });
