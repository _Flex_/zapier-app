'use strict';
const should = require('should');

const zapier = require('zapier-platform-core');

const App = require('../index');
const appTester = zapier.createAppTester(App);

//These are automated tests for the Issue create and Issue Trigger.
//They will run every time the `zapier test` command is executed.
describe('note trigger', () => {
    zapier.tools.env.inject();

    // Make sure there's an open issue to fetch here!
    it('should get a notes', (done) => {
        const bundle = {
            authData: {
                apiKey: process.env.TEST_API_KEY
            }
        };
        appTester(App.triggers.note.operation.perform, bundle)
            .then((response) => {
                response.should.be.an.instanceOf(Array);
                done();
            })
            .catch(done);
    });

    it('should create a new note', (done) => {
        const bundle = {
            authData: {
                apiKey: process.env.TEST_API_KEY
            },
            inputData: {
                title: 'Test Note',
                body: 'This is a test note created from an automated test for the Zapier Example App'
            }
        };
        appTester(App.creates.note.operation.perform, bundle)
            .then((response) => {
                should.exists(response.title);
                should.exists(response.text);
                should.exists(response.id);
                done();
            })
            .catch(done);
    });
});
