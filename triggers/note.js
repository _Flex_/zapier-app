const sample = require('../samples/sample_note');
const url = 'http://ec2-18-221-73-13.us-east-2.compute.amazonaws.com:8000';

const triggerIssue = (z, bundle) => {
    const responsePromise = z.request({
        method: 'GET',
        url: `${process.env.API_URL || url}/notes/`
    });
    return responsePromise
        .then(response => JSON.parse(response.content));
};

module.exports = {
    key: 'note',
    noun: 'Note',

    display: {
        label: 'Get Note',
        description: 'Triggers on a new note.'
    },

    operation: {
        inputFields: [],
        perform: triggerIssue,

        sample: sample
    }
};
